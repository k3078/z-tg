在我看来这是一个比较说人话的第三方封装API。就是相关文档有些少。

项目地址： https://github.com/eternnoir/pyTelegramBotAPI

文档： https://pytba.readthedocs.io/en/latest/index.html （英文）

之前我们已经尝试了最基本的送信功能，但却没有实现互动。学习使用API，我们能添加更多互动/命令功能。

## 开始使用&简单的复读机机器人

安装

```shell
pip install pyTelegramBotAPI
```

新建项目：

```py
import telebot

# 实例化机器人，其中TOKEN换成你自己的。
bot = telebot.TeleBot("TOKEN", parse_mode=None) # You can set parse_mode by default. HTML or MARKDOWN

# 修饰符，表明此函数处理命令/start和/help
@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    # 回复
	bot.reply_to(message, "Howdy, how are you doing?")

@bot.message_handler(func=lambda m: True)
def echo_all(message):
    # massage.text获取内容
	bot.reply_to(message, message.text)

# 开始永续运行
bot.infinity_polling()
```

## 配置 Menu 菜单

## 配置 Inline/Makeup 交互式消息

