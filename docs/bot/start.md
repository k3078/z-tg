创建TGbot并且使用TGbot发送消息非常简单

## 创建bot


1. 要在 tg 中打开与 [TelegramBotFather](https://telegram.me/botfather) 的对话，点击/start。
2. 点击Menu-/newbot，你也可以手动输入 /newbot 的指令。
3. 输入bot的昵称，这是展示给用户的名字，可以不唯一。
4. 随后输入用户名（需要以bot结尾），也就是机器人的ID，必须唯一。

随后我们将收到一段信息，其中包括了如下部分：

```
Done! Congratulations on your new bot. You will find it at t.me/zhelper1_bot. You can now add a description, about section and profile picture for your bot, see /help for a list of commands. By the way, when you've finished creating your cool bot, ping our Bot Support if you want a better username for it. Just make sure the bot is fully operational before you do this.

Use this token to access the HTTP API:
324155656:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
Keep your token secure and store it safely, it can be used by anyone to control your bot.

For a description of the Bot API, see this page: https://core.telegram.org/bots/api
```

复制中间`324155656:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX`这一段，保存好，**这是机器人的token**，拥有此token，就有对bot的绝对访问权限，如果泄露可能有被盗用的风险。

## helloworld！

接下来我们尝试使用bot对自己发送helloworld消息。

首先我们要获取相对于bot（或其他人）来说，你的chat_id。这里有两种方法

### 使用GETIDbot

搜索 @getidsbot 开启聊天，将会返回如下值：

```
Hello, roy! I am a computer program that can chat (also known as a 'bot') and I can tell you something about you and the messages you send me.

Try it out, just forward me some messages ;)

Note from the developers:
This is a very early version of the bot. Therefore a few things (explainatory texts, etc.) are still missing. If you can code, here is the GitHub repo (https://github.com/wjclub/telegram-bot-getids). If you don't know how to program, you can still help. Just tell us @wjclub.

👤 You
 ├ id: 198XXXXXXX
 ├ is_bot: false
 ├ first_name: roy
 └ language_code: zh-hans (-)
```

复制中间的id一栏即可，**这是你的chatid**。

### 使用botUPdate获取

使用BOT用户名搜索你刚才创建的bot，随便说一句话。

用你的 token 替换掉下面的toen，得到一个链接，这是获取Bot所收到的最新消息的API。

```
https://api.telegram.org/bot324155656:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/getUpdates
```

我们可以直接在浏览器中访问它，返回结果如下：

```
{"ok":true,"result":[{"update_id":848599183,
"message":{"message_id":2,"from":{"id":19831XXXXX,"is_bot":false,"first_name":"roy","username":"roydhdd","language_code":"zh-hans"},"chat":{"id":19831XXXXX,"first_name":"roy","username":"roydhdd","type":"private"},"date":1650781371,"text":"test"}}]}
```

从中获取 chat id，例如此处为 19831XXXXX，**这是你的chatid**。

### 发送helloworld

按照以上配置中的chatid和token，我们可以直接在浏览器中访问以下链接给自己发送消息。

```shell
# 注意修改成你自己的chatid和token
https://api.telegram.org/bot{token}/sendMessage?chat_id={chatid}&text={massage}

# 示例
https://api.telegram.org/bot324155656:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX/sendMessage?chat_id=19831XXXXX&text=helloworld
```

访问此链接，TG上应当就能够收到消息了。

## 在Python中集成

由于大部分第三方 TGBOT SDK 库都基于 python，这里也使用 python 做一个简单的演示。

注意requests库要求关闭代理运行

```python
import requests

# 需要发送的消息
massage='test'

# 向TG发送消息
baseuri = 'https://api.telegram.org/bot'
method = '/sendMessage?chat_id='
chat_id = "19831XXXXX"  # 需要发送的Userid
token = "324155656:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"  # 机器人 TOKEN

uri = baseuri+token+method+chat_id+'&text='+massage

requests.get(uri)
```

## 说明

TGbot仅仅是一个接口，基于此接口，可以开发出非常多有意思、实用的小功能。您可以参考 实例 部分，了解TGbot的应用。**强烈推荐先对python有一个大概的了解。**